# Documentação webhook

Existem dois tipo de webhook: vendas e contratos.

No caso de webhook de vendas, o tipo do evento (type) é "sale" e o evento é "saleUpdated" indicando que a transação teve seu status alterado.

Campos:

**oldStatus**\
Descrição: indica qual o status anterior da venda.
Valores possíveis: created, paid, waiting_payment

**currentStatus**\
Descrição: indica o status atual da venda.
Valores possíveis: paid, refused, refunded, chargedback, waiting_payment

**product.affiliation_proposal**\
Descrição: permite que o afiliado possa gerar propostas.

**product.allow_proposal**\
Descrição: permite ao produto ter propostas

**product.is_active**\
Descrição: indica se o produto está ativo ou não.

**product.thank_you_page**\
Descrição: página que o usuário será redirecionado após comprar o produto.

**product.affiliation_public**\
Descrição: permite que o produto em questão possa ser exibido na vitrine para que os afiliados possam solicitar afiliação.

**product.affiliation_approbation**\
Descrição: permite que as solicitações de afiliação passem pro aprovação do vendedor ou não.

**product.method**\
Descrição: método de pagamento permitido para este produto.
Valores possíveis: CREDIT_CARD, TWO_CREDIT_CARDS, BOLETO, PIX,PAYPAL (ou todos eles)

**product.type**\
Descrição: indica qual o tipo do produto: simples ou assinatura.
Valores possíveis: TRANSACTIOM, SUBSCRIPTION

```json
{
   "oldStatus":"",
   "currentStatus":"",
   "type":"sale",
   "event":"saleUpdated",
   "product":{
      "affiliation_proposal":1,
      "description":"",
      "allow_proposal":0,
      "created_at":"",
      "is_active":1,
      "period":0,
      "updated_at":"",
      "thank_you_page":"",
      "amount":99,
      "id":0,
      "affiliation_public":1,
      "affiliation_approbation":1,
      "proposal_minimum":0,
      "type":"",
      "method":"",
      "name":""
   },
   "sale":{
      "status":"paid",
      "seller_id":0,
      "created_at":"",
      "updated_at":"",
      "method":"",
      "amount":99,
      "client_id":0,
      "installments":1,
      "type":"",
      "id":0
   },
   "seller":{
      "cellphone":"",
      "email":"",
      "name":"",
      "id":0
   },
   "client":{
      "city":"",
      "neighborhood":"",
      "name":"",
      "cellphone":"",
      "updated_at":"",
      "created_at":"",
      "number":"",
      "zipcode":"",
      "email":"",
      "street":" ",
      "cpf_cnpj":"",
      "complement":"",
      "uf":"",
      "id":0
   }
}
```

Webhook de contratos:

Campos:

**oldStatus**\
Descrição: indica qual o status anterior do contrato.
Valores possíveis: created, paid, trialing

**currentStatus**\
Descrição: indica o status atual do contrato.
Valores possíveis: paid, trialing, pending_payment, unpaid, canceled

**contract.start_date**\
Descrição: data de início do contrato.

**contract.current_period_end**\
Descrição: data do vencimento da mensalidade atual.

**productMetas**\
Descrição: metas do produto, se houver.

**proposalMetas**\
Descrição: metas da proposta, se houver.

```json
{
   "oldStatus":"",
   "currentStatus":"",
   "type":"contract",
   "event":"contractUpdated",
   "product":{
      "affiliation_proposal":1,
      "description":"",
      "allow_proposal":0,
      "created_at":"",
      "is_active":1,
      "period":0,
      "updated_at":"",
      "thank_you_page":"",
      "amount":0,
      "id":0,
      "affiliation_public":1,
      "affiliation_approbation":1,
      "proposal_minimum":0,
      "type":"",
      "method":"",
      "name":""
   },
   "currentSale":{
      "status":"paid",
      "seller_id":0,
      "created_at":"",
      "updated_at":"",
      "method":"",
      "amount":0,
      "client_id":0,
      "installments":1,
      "type":"",
      "id":0
   },
   "contract":{
      "start_date":"",
      "created_at":"",
      "updated_at":"",
      "status":"",
      "current_period_end":"",
      "id":0
   },
   "seller":{
      "cellphone":"",
      "email":"",
      "name":"",
      "id":0
   },
   "affiliate":{
      "cellphone":"",
      "email":"",
      "name":"",
      "id":0
   },
   "client":{
      "city":"",
      "neighborhood":"",
      "name":"",
      "cellphone":"",
      "updated_at":"",
      "created_at":"",
      "number":"",
      "zipcode":"",
      "email":"",
      "street":" ",
      "cpf_cnpj":"",
      "complement":"",
      "uf":"",
      "id":0
   },
   "productMetas":[
     {
      "key":"",
      "value":"",
      "id":0
   	 }
   ],
   "proposalMetas":[
     {
      "key":"",
      "value":"",
      "id":0
   	 }
   ],
}
```